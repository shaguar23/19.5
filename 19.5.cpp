﻿#include <iostream>

class Animals
{
public:
	virtual void Voice() const = 0;
};

class Cat : public Animals
{
public:
	void Voice() const override
		{
		std::cout << "Cat say: ";
		std::cout << "Meow\n";
		}
};
class Dog : public Animals
{
public:
	void Voice()  const override
		{
		std::cout << "Dog say: ";
		std::cout << "Woof\n";
		}
};

class Fox : public Animals
{
public:
	void Voice() const override
	{
		std::cout << "What does the Fox say?: ";
		std::cout << "Ring-ding-ding-ding-dingeringeding";
	}
};
int main()
{
	Animals* animal[3];
	animal[0] = new Cat();
	animal[1] = new Dog();
	animal[2] = new Fox();

	for (Animals* p : animal)
	{
		p->Voice();
	}
};